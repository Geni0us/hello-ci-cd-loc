<?php

namespace humans;

class Somebody
{
    /**
     * @var string
     */
    private $name = "";

    public function __construct($name)
    {
        $this->name = $name;
    }

    public static function sayHello()
    {
        return "Hello!";
    }

    public function introduce()
    {
        return "My name is {$this->name}.";
    }
}
